package mn.school.entity;
import javax.persistence.Column;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Score {
		public Score(String id, double firstSemesterScore, double secondSemesterScore, double thirdSemesterScore,
			double fourthSemesterScore, double finalScore, int year) {
		super();
		this.id = id;
		this.firstSemesterScore = firstSemesterScore;
		this.secondSemesterScore = secondSemesterScore;
		this.thirdSemesterScore = thirdSemesterScore;
		this.fourthSemesterScore = fourthSemesterScore;
		this.finalScore = finalScore;
		this.year = year;
	}
		public Score() {
		super();
		// TODO Auto-generated constructor stub
	}
		@Id
		private String id;
		@Column
		private double firstSemesterScore;
		@Column
		private double secondSemesterScore;
		@Column
		private double thirdSemesterScore;
		@Column
		private double fourthSemesterScore;
		@Column
		private double finalScore;
		@Column
		private int year;
		@ManyToOne
		@JoinColumn(name = "subjectId")
		private Subject subjects;
		@ManyToOne
		@JoinColumn(name = "studentId")
		private Student students;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public double getFirstSemesterScore() {
			return firstSemesterScore;
		}
		public void setFirstSemesterScore(double firstSemesterScore) {
			this.firstSemesterScore = firstSemesterScore;
		}
		public double getSecondSemesterScore() {
			return secondSemesterScore;
		}
		public void setSecondSemesterScore(double secondSemesterScore) {
			this.secondSemesterScore = secondSemesterScore;
		}
		public double getThirdSemesterScore() {
			return thirdSemesterScore;
		}
		public void setThirdSemesterScore(double thirdSemesterScore) {
			this.thirdSemesterScore = thirdSemesterScore;
		}
		public double getFourthSemesterScore() {
			return fourthSemesterScore;
		}
		public void setFourthSemesterScore(double fourthSemesterScore) {
			this.fourthSemesterScore = fourthSemesterScore;
		}
		public double getFinalScore() {
			return finalScore;
		}
		public void setFinalScore(double finalScore) {
			this.finalScore = finalScore;
		}
		public int getYear() {
			return year;
		}
		public void setYear(int year) {
			this.year = year;
		}
		public Subject getSubjects() {
			return subjects;
		}
		public void setSubjects(Subject subjects) {
			this.subjects = subjects;
		}
		public Student getStudents() {
			return students;
		}
		public void setStudents(Student students) {
			this.students = students;
		}

}

