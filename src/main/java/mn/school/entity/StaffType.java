package mn.school.entity;
import javax.persistence.Column
;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class StaffType {
	
	@Id 
	private String id;
	@Column 
	private String name;
	
	public StaffType() {
		super();
		// TODO Auto-generated constructor stub
	}
	public StaffType(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
