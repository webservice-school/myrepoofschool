package mn.school.entity;
import javax.persistence.Column;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Student {
	
	@Id 
	private String code;
	@Column
	private String firstName; 
	@Column
	private String lastName;
	@Column
	private String motherName;
	@Column
	private Date dateOfBirth;
	@Column
	private String registerNumber;
	@Column
	private String gender;
	@Column
	private String placeOfBirth;
	@Column
	private String phoneNumber;
	@Column
	private String address;
	@Column
	private String fatherProfession;
	@Column
	private String fatherPhoneNumber;
	@Column
	private String motherPhoneNumber;
	
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Student(String code, String name, String lastName, String motherName, Date dateOfBirth,
			String registerNumber, String gender, String placeOfBirth, String phoneNumber, String address,
			String fatherProfession, String fatherPhoneNumber, String motherPhoneNumber) {
		super();
		this.code = code;
		this.firstName = name;
		this.lastName = lastName;
		this.motherName = motherName;
		this.dateOfBirth = dateOfBirth;
		this.registerNumber = registerNumber;
		this.gender = gender;
		this.placeOfBirth = placeOfBirth;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.fatherProfession = fatherProfession;
		this.fatherPhoneNumber = fatherPhoneNumber;
		this.motherPhoneNumber = motherPhoneNumber;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return firstName;
	}
	public void setName(String name) {
		this.firstName = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getRegisterNumber() {
		return registerNumber;
	}
	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getFatherProfession() {
		return fatherProfession;
	}
	public void setFatherProfession(String fatherProfession) {
		this.fatherProfession = fatherProfession;
	}
	public String getFatherPhoneNumber() {
		return fatherPhoneNumber;
	}
	public void setFatherPhoneNumber(String fatherPhoneNumber) {
		this.fatherPhoneNumber = fatherPhoneNumber;
	}
	public String getMotherPhoneNumber() {
		return motherPhoneNumber;
	}
	public void setMotherPhoneNumber(String motherPhoneNumber) {
		this.motherPhoneNumber = motherPhoneNumber;
	}
	
	
}
