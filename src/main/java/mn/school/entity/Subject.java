package mn.school.entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
@Entity
public class Subject {
	@Id 
	private String id;
	@Column 
	private String name;
	@ManyToMany
	@JoinTable(
			name = "grade_subject",
			joinColumns=@JoinColumn(name="subject_id"),
			inverseJoinColumns=@JoinColumn(name="grade_id")
			)
	private List<Grade> grades;
	public Subject() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Subject(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
