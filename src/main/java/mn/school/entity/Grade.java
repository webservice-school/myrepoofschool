package mn.school.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;
import javax.persistence.Id;
import javax.persistence.Entity;
@Entity
public class Grade {
	@Id 
	private String id;
	@Column 
	private String name;
	@ManyToMany
	@JoinTable(
			name = "grade_subject",
			joinColumns=@JoinColumn(name="grade_id"),
			inverseJoinColumns=@JoinColumn(name="subject_id")
			)
	private List<Subject> subjects;
	public Grade() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Grade(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
