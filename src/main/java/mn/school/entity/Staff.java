package mn.school.entity;
import javax.persistence.Column;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Staff {
	public Staff() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Staff(String code, String name, Date dateOfBirth, String lastName, String gender, String phoneNumber,
			String address, int dateOfHiring, int yearOfService, double salary) {
		super();
		this.code = code;
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.lastName = lastName;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.dateOfHiring = dateOfHiring;
		this.yearOfService = yearOfService;
		this.salary = salary;
	}
	@Id
	private String code;
	@Column
	private String name;
	@Column
	private Date dateOfBirth;
	@Column
	private String lastName;
	@Column
	private String gender;
	@Column
	private String phoneNumber;
	@Column
	private String address;
	@Column
	private int dateOfHiring;
	@Column
	private int yearOfService;
	@Column
	private double salary;
	
	@ManyToOne
	@JoinColumn(name = "staffTypeId")
	private StaffType staffType;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getDateOfHiring() {
		return dateOfHiring;
	}
	public void setDateOfHiring(int dateOfHiring) {
		this.dateOfHiring = dateOfHiring;
	}
	public int getYearOfService() {
		return yearOfService;
	}
	public void setYearOfService(int yearOfService) {
		this.yearOfService = yearOfService;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
}
