package mn.school.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import mn.school.entity.Subject;
@Repository
public interface SubjectRepository extends JpaRepository<Subject,String> {

}
