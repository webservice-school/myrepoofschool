package mn.school.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import mn.school.entity.Grade;
import mn.school.entity.Student;
public interface GradeRepository extends JpaRepository<Grade,String> {
	@Query
	List<Grade> findById(String id);
}
