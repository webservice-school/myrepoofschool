package mn.school.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import mn.school.entity.Staff;
import mn.school.entity.Student;
@Repository
public interface StaffRepository extends JpaRepository<Staff,String>{
	@Query
	List<Staff> findByStaffTypeId(String staffTypeId);
	@Query
	List<Staff> findByName(String name);
}
