package mn.school.controller;
import mn.school.entity.Student;


import mn.school.service.StudentServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
public class StudentController {
	@Autowired
	StudentServiceImpl studentServiceImpl;
	
	public StudentController() {
	}
	
	@RequestMapping("/")
    @ResponseBody
    String home() {
        return "Hello World!";
    }

	@RequestMapping(value = "/students/", method=RequestMethod.GET)
    @ResponseBody
    List<Student> listStudents() {
        return studentServiceImpl.getStudents();
    }

	@RequestMapping(value="/students/name/{firstName}", method=RequestMethod.GET)
    @ResponseBody
    List<Student> getStudentsByFirstName(@PathVariable("firstName") String name) {
		return studentServiceImpl.getStudentsByFirstName(name);
    }
	
	
	
	@RequestMapping(value="/students/{code}", method=RequestMethod.GET)
    @ResponseBody
    Student getStudent(@PathVariable("code") String code) {
		return studentServiceImpl.getStudent(code);
    }

	@RequestMapping(value="/students/{code}", method=RequestMethod.DELETE)
    @ResponseBody
    void removeStudent(@PathVariable("code") String code) {
		studentServiceImpl.deleteStudent(code);
    }

	@RequestMapping(value="/students/", method=RequestMethod.POST)
    @ResponseBody
    Student addStudent(@RequestBody Student student) {
		//Student s = studentServiceImpl.getStudent(student.getCode());
		//if (s== null) {
			return studentServiceImpl.saveStudent(student);
		//}
		//return null;
    }

	@RequestMapping(value="/students/{code}", method=RequestMethod.PUT)
    @ResponseBody
    Student editStudent(@PathVariable("code") String code, @RequestBody Student student) {
		//if (studentServiceImpl.getStudent(code) != null) {
			return studentServiceImpl.saveStudent(student);
		//}
		//return null;
    }
}
