package mn.school.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;

import mn.school.entity.Staff;
import mn.school.service.StaffServiceImpl;
@Controller 
public class StaffController {
	@Autowired
	StaffServiceImpl staffServiceImpl;
	
	public StaffController() {
	}
	

	@RequestMapping(value = "/staffs/", method=RequestMethod.GET)
    @ResponseBody
    List<Staff> listStaffs() {
        return staffServiceImpl.getStaffs();
    }

	@RequestMapping(value="/staffs/name/{firstName}", method=RequestMethod.GET)
    @ResponseBody
    List<Staff> getStaffsByFirstName(@PathVariable("firstName") String name) {
		return staffServiceImpl.getStaffsByName(name);
    }
	

	@RequestMapping(value="/staffs/staffTypeId/{code}", method=RequestMethod.GET)
    @ResponseBody
    List<Staff> getStudentsByCode(@PathVariable("code") String name) {
		return staffServiceImpl.getStaffsByStaffTypeId(name);
    }
	
	@RequestMapping(value="/staffs/{code}", method=RequestMethod.GET)
    @ResponseBody
    Staff getStaff(@PathVariable("code") String code) {
		return staffServiceImpl.getStaff(code);
    }

	@RequestMapping(value="/staffs/{code}", method=RequestMethod.DELETE)
    @ResponseBody
    void removeStaff(@PathVariable("code") String code) {
		staffServiceImpl.deleteStaff(code);
    }
	
	@RequestMapping(value="/staffs/", method=RequestMethod.POST)
    @ResponseBody
    Staff addStaff(@RequestBody Staff Staff) {
		//Staff s = StaffServiceImpl.getStaff(Staff.getCode());
		//if (s== null) {
			return staffServiceImpl.saveStaff(Staff);
		//}
		//return null;
    }

	@RequestMapping(value="/staffs/{code}", method=RequestMethod.PUT)
    @ResponseBody
    Staff editStaff(@PathVariable("code") String code, @RequestBody Staff Staff) {
		//if (StaffServiceImpl.getStaff(code) != null) {
			return staffServiceImpl.saveStaff(Staff);
		//}
		//return null;
    }
}
