package mn.school.service;

import java.util.List;

import mn.school.entity.Grade;

public interface GradeService {
	
	public List<Grade> getGradesById(String id);
	public Grade getId(String theId);

	public Grade saveGrade(Grade theGrade);

	public void deleteId(String theId);
}
