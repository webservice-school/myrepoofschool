package mn.school.service;
import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mn.school.entity.Staff;
import mn.school.repository.StaffRepository;

@Service
public class StaffServiceImpl implements StaffService {
	@Autowired
	StaffRepository staffRepository;
	@Override
	public List<Staff> getStaffs() {
		return staffRepository.findAll();
	}

	@Override
	public Staff getStaff(String theId) {
		return staffRepository.getOne(theId);
	}

	@Override
	public Staff saveStaff(Staff theStudent) {
		return staffRepository.save(theStudent);
	}

	@Override
	public void deleteStaff(String theId) {
		staffRepository.delete(theId);
	}

	@Override
	public List<Staff> getStaffsByName(String name) {
		// TODO Auto-generated method stub
		return staffRepository.findByName(name);
	}
	@Override
	public List<Staff> getStaffsByStaffTypeId(String staffTypeId) {
		// TODO Auto-generated method stub
		return staffRepository.findByStaffTypeId(staffTypeId);
	}
}
