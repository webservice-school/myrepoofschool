package mn.school.service;
import java.util.List;

import mn.school.entity.Student;
public interface StudentService {
	public List<Student> getStudents();
	
	public List<Student> getStudentsByFirstName(String firstName);
	
	
	
	public Student getStudent(String theId);

	public Student saveStudent(Student theStudent);

	public void deleteStudent(String theId);
}
