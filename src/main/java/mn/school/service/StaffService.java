package mn.school.service;
import java.util.List;
import mn.school.entity.Staff;
public interface StaffService {
	public List<Staff> getStaffs();
	
	public List<Staff> getStaffsByName(String name);
	public List<Staff> getStaffsByStaffTypeId(String staffTypeId);
	
	
	public Staff getStaff(String theId);

	public Staff saveStaff(Staff theStaff);

	public void deleteStaff(String theId);
}
