package mn.school.service;
import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mn.school.entity.Student;
import mn.school.repository.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService{
	@Autowired
	StudentRepository studentRepository;
	@Override
	public List<Student> getStudents() {
		return studentRepository.findAll();
	}

	@Override
	public Student getStudent(String theId) {
		return studentRepository.getOne(theId);
	}

	@Override
	public Student saveStudent(Student theStudent) {
		return studentRepository.save(theStudent);
	}

	@Override
	public void deleteStudent(String theId) {
		studentRepository.delete(theId);
	}

	@Override
	public List<Student> getStudentsByFirstName(String firstName) {
		// TODO Auto-generated method stub
		return studentRepository.findByFirstName(firstName);
	}
}
